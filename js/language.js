const initialLanguage = "Nederlands";

const languageElements = {
    /*ONE*/
    /*NAVBAR*/
    nav_two: document.querySelector("#nav_two"),
    nav_three: document.querySelector("#nav_three"),
    nav_four: document.querySelector("#nav_four"),
    nav_five: document.querySelector("#nav_five"),
    nav_six: document.querySelector("#nav_six"), /*CONTENT*/
    title: document.querySelector("#title"),
    subtitle_1: document.querySelector("#subtitle_1"),
    subtitle_2: document.querySelector("#subtitle_2"),

    /*TWO*/
    /*LEFT*/
    left_1: document.querySelector("#left_1"),
    left_2: document.querySelector("#left_2"),
    left_3: document.querySelector("#left_3"),
    left_4: document.querySelector("#left_4"),
    left_5: document.querySelector("#left_5"),
    left_6: document.querySelector("#left_6"), /*RIGHT*/
    right_1: document.querySelector("#right_1"),
    right_2: document.querySelector("#right_2"),
    right_3: document.querySelector("#right_3"),
    right_4: document.querySelector("#right_4"),
    right_5: document.querySelector("#right_5"),

    /*THREE*/
    /*LIST 1*/
    list_1_title: document.querySelector("#list_1_title"),
    list_1_1: document.querySelector("#list_1_1"),
    list_1_2: document.querySelector("#list_1_2"),
    list_1_3: document.querySelector("#list_1_3"),
    list_1_4: document.querySelector("#list_1_4"),
    list_1_5: document.querySelector("#list_1_5"),
    list_1_6: document.querySelector("#list_1_6"),
    list_1_7: document.querySelector("#list_1_7"), /*LIST 2*/
    list_2_title: document.querySelector("#list_2_title"),
    list_2_1: document.querySelector("#list_2_1"),
    list_2_2: document.querySelector("#list_2_2"),
    list_2_3: document.querySelector("#list_2_3"),
    list_2_4: document.querySelector("#list_2_4"),
    list_2_5: document.querySelector("#list_2_5"),
    list_2_6: document.querySelector("#list_2_6"),
    list_2_7: document.querySelector("#list_2_7"),
    list_2_8: document.querySelector("#list_2_8"),
    list_2_9: document.querySelector("#list_2_9"),
    list_2_10: document.querySelector("#list_2_10"),
    list_2_11: document.querySelector("#list_2_11"),
    list_2_12: document.querySelector("#list_2_12"),
    list_2_13: document.querySelector("#list_2_13"),
    list_2_14: document.querySelector("#list_2_14"),
    list_2_15: document.querySelector("#list_2_15"), /*LIST 3*/
    list_3_title: document.querySelector("#list_3_title"),
    list_3_1: document.querySelector("#list_3_1"),
    list_3_2: document.querySelector("#list_3_2"),
    list_3_3: document.querySelector("#list_3_3"),
    list_3_4: document.querySelector("#list_3_4"),
    list_3_5: document.querySelector("#list_3_5"),
    list_3_6: document.querySelector("#list_3_6"),
    list_3_7: document.querySelector("#list_3_7"),
    list_3_8: document.querySelector("#list_3_8"),
    list_3_9: document.querySelector("#list_3_9"),
    list_3_10: document.querySelector("#list_3_10"),
    list_3_11: document.querySelector("#list_3_11"),

    /*FOUR*/
    four_1_title: document.querySelector("#four_1_title"),
    four_1_body_1: document.querySelector("#four_1_body_1"),
    four_1_body_2: document.querySelector("#four_1_body_2"),
    four_1_body_3: document.querySelector("#four_1_body_3"),
    four_2_title: document.querySelector("#four_2_title"),
    four_2_body_1: document.querySelector("#four_2_body_1"),
    four_2_body_2: document.querySelector("#four_2_body_2"),
    four_2_body_3: document.querySelector("#four_2_body_3"),

    /*FIVE*/
    value_1_title: document.querySelector("#value_1_title"),
    value_1_content: document.querySelector("#value_1_content"),
    value_2_title: document.querySelector("#value_2_title"),
    value_2_content: document.querySelector("#value_2_content"),
    value_3_title: document.querySelector("#value_3_title"),
    value_3_content: document.querySelector("#value_3_content"),
    value_4_title: document.querySelector("#value_4_title"),
    value_4_content: document.querySelector("#value_4_content"),

    /*SIX*/
    contact_title: document.querySelector("#contact_title"),
    contact_subtitle: document.querySelector('#contact_subtitle'),
    address: document.querySelector('#address'),
    phone: document.querySelector('#phone'),
    email: document.querySelector('#email'),
};

const data = {
    Nederlands: {
        /*ONE*/
        /*NAVBAR*/
        nav_two: "Over",
        nav_three: "Opdrachten",
        nav_four: "Ervaring",
        nav_five: "Waarden",
        nav_six: "Contact", /*CONTENT*/
        title: "Passie voor Food & People",
        subtitle_1: "Maar ook",
        subtitle_2: "meten is weten",

        /*TWO*/
        /*LEFT*/
        left_1: "Ons credo luidt niet voor niets",
        left_2: "\“Hoe kunnen wij u helpen,<br>uw doelstellingen te realiseren\”",
        left_3: "Heeft uw organisatie nood aan professioneel advies of begeleiding, procesoptimalisatie of marktinzichten?",
        left_4: "Wenst u betere operationele resultaten, meer omzet, meer winst?",
        left_5: "Een hogere klant- en/of medewerker tevredenheid? Of bent u op zoek naar iemand die uw food&amp;beverage activiteiten kan toetsen aan de markt en verbeteringsvoorstellen doet of implementeert?",
        left_6: "Wij helpen u of uw organisatie graag verder!", /*RIGHT*/
        right_1: "Onze betrokkenheid, oprechte communicatie &amp; entrepreneurship, maken van ons een betrouwbare partner in de sector.",
        right_2: "U kan bij ons terecht voor marktinzichten, contract- of tenderbegeleiding, benchmarks, interim- management, conceptanalyses, rentabiliteitsanalyses, sales ondersteuning, inkoop consultancy, mystery visits, haccp support, eventbegeleiding en zoveel meer.",
        right_3: "Samen met u onderzoeken we uw behoeften en werken we oplossingen uit op maat. Hierbij adviseren we u graag vanuit onze uitgebreide praktijkkennis, zonder daarbij uw doelstellingen uit het oog te verliezen.",
        right_4: "Neem zeker een kijkje op onze website en aarzel niet ons te contacteren voor meer informatie of een verkennend gesprek.",
        right_5: "Tot binnenkort!",

        /*THREE*/
        /*LIST 1*/
        list_1_title: "Voor wie",
        list_1_1: "Multinationals of KMO’s met cateringactiviteiten of -behoeften",
        list_1_2: "Real Estate bedrijven",
        list_1_3: "Gebouweigenaars of -beheerders",
        list_1_4: "Facilitaire bedrijven",
        list_1_5: "Retailers",
        list_1_6: "Contract cateraars",
        list_1_7: "Horecabedrijven", /*LIST 2*/
        list_2_title: "Type opdrachten",
        list_2_1: "Marktinzichten en -analyses",
        list_2_2: "Marktconformiteitstoets & quick scan",
        list_2_3: "Contract begeleiding",
        list_2_4: "Tender begeleiding",
        list_2_5: "Interim-management",
        list_2_6: "Conceptanalyses",
        list_2_7: "Praktische concept ondersteuning",
        list_2_8: "Rentabiliteitsanalyses",
        list_2_9: "Sales ondersteuning & -advies",
        list_2_10: "Inkoopondersteuning & -advies",
        list_2_11: "Optimalisatie zowel conceptueel, operationeel als financieel",
        list_2_12: "Inrichting en flows (looplijnen & wachtrijen)",
        list_2_13: "HACCP ondersteuning",
        list_2_14: "Mystery visits",
        list_2_15: "Eventbegeleiding", /*LIST 3*/
        list_3_title: "Toegevoegde waarde",
        list_3_1: "Happy people",
        list_3_2: "Tevreden klanten en medewerkers",
        list_3_3: "Future proof concepten",
        list_3_4: "Marktconform aanbod",
        list_3_5: "Marktconforme prijzen",
        list_3_6: "Betere operationele resultaten",
        list_3_7: "Hogere klanttevredenheid",
        list_3_8: "Meer kwaliteit",
        list_3_9: "Meer omzet",
        list_3_10: "Meer winst",
        list_3_11: "Tijd voor andere prioriteiten",

        /*FOUR*/
        four_1_title: "Uitgebreide ervaring in catering",
        four_1_body_1: "Meer dan 20 jaar ervaring in operationele en commerciële functies",
        four_1_body_2: "bij verschillende internationale cateraars in België en Nederland.",
        four_1_body_3: "Functies & verantwoordelijkheden als bedrijfsmanager, commercieel directeur, operationeel manager, …",
        four_2_title: "Uitgebreide ervaring in horeca",
        four_2_body_1: "Meer dan 12 jaar ervaring in operationele functies",
        four_2_body_2: "in verschillende internationale hotelketens bij verschillende internationale cateraars in België en Nederland.",
        four_2_body_3: "Functies & verantwoordelijkheden als F&B manager, banquet&event manager, hoofd restaurants, Inkoop, cost control, restaurant MdH, keuken & zaal functies.",


        /*FIVE*/
        value_1_title: "Betrokken",
        value_1_content: "Door je echt in te leven in de klant en zijn project, krijg je commitment en engagement. Je begrijpt jouw klant en zijn bedrijfscultuur. Zo creëer je klantgerichte oplossingen die echt toegevoegde waarde betekenen",
        value_2_title: "Oprecht & rechtuit",
        value_2_content: "Het cliché luidt dat consultants klanten naar de mond praten. Klanten die investeren, wensen veel meer dan dat. Daar is moed voor nodig. Het is niet vanzelfsprekend om te zeggen waar het op staat",
        value_3_title: "Nieuwsgierig",
        value_3_content: "Het cliché luidt dat consultants klanten naar de mond praten. Klanten die investeren, wensen veel meer dan dat. Daar is moed voor nodig. Het is niet vanzelfsprekend om te zeggen waar het op staat",
        value_4_title: "Ondernemend & Teamspeler",
        value_4_content: "Met een gedegen plan van aanpak haal je de deadlines, lever je topkwaliteit en bewaak je de voortgang. Uitkijkend naar mensen die er voldoening uit halen samen met anderen een project te realiseren",

        /*SIX*/
        contact_title: "Neem contact op",
        contact_subtitle: "Mail ons of bel ons! Wij helpen u graag verder.",
        address: "Adres",
        phone: "Telefoon",
        email: "Email",
    },

    English: {
        /*ONE*/
        /*NAVBAR*/
        nav_two: "About",
        nav_three: "Assignments",
        nav_four: "Experience",
        nav_five: "Values",
        nav_six: "Contact", /*CONTENT*/
        title: "Passion for Food & People",
        subtitle_1: "But also",
        subtitle_2: "measuring is knowing",

        /*TWO*/
        /*LEFT*/
        left_1: "Our credo is therefore",
        left_2: "\“How can we help you,<br>achieve your goals\”",
        left_3: "Does your organization need professional advice or support, process optimization or market insights?",
        left_4: "Do you want better operational results, more turnover, more profit?",
        left_5: "Higher customer and/or employee satisfaction? Or are you looking for someone who can benchmark your food&amp;beverage activities and make or implement improvements?",
        left_6: "We are happy to help you or your organization!", /*RIGHT*/
        right_1: "Our commitment, straightforward communication & entrepreneurship, make us a reliable partner in the sector.",
        right_2: "You can contact us for market insights, contract management, tender support, benchmarks, interim management, concept analyses, profitability analyses, sales support, procurement consultancy, mystery visits, haccp assistance, event management and so much more.",
        right_3: "Together with you, we examine your needs and develop tailor-made solutions. We are happy to advise you based on our extensive practical expertise, without losing sight of your goals.",
        right_4: "Have a look at our website and do not hesitate to contact us for more information or an exploratory meeting.",
        right_5: "See you soon!",

        /*THREE*/
        /*LIST 1*/
        list_1_title: "For whom",
        list_1_1: "Multinationals or SMEs with catering activities or needs",
        list_1_2: "Real Estate companies",
        list_1_3: "Building owners or managers",
        list_1_4: "Facility companies",
        list_1_5: "Retailers",
        list_1_6: "Contract caterers",
        list_1_7: "Hospitality companies", /*LIST 2*/
        list_2_title: "Type of assignments",
        list_2_1: "Market insights and analyses",
        list_2_2: "Market conformity test & quick scan",
        list_2_3: "Contract support",
        list_2_4: "Tender support",
        list_2_5: "Interim management",
        list_2_6: "Concept analyses",
        list_2_7: "Practical concept support",
        list_2_8: "Profitability analyses",
        list_2_9: "Sales support & advice",
        list_2_10: "Procurement support & advice",
        list_2_11: "Optimization both conceptually, operationally and financially",
        list_2_12: "Design and flows (walking lines & queues)",
        list_2_13: "HACCP support",
        list_2_14: "Mystery visits",
        list_2_15: "Event guidance", /*LIST 3*/
        list_3_title: "Added value",
        list_3_1: "Happy people",
        list_3_2: "Satisfied customers and employees",
        list_3_3: "Future proof concepts",
        list_3_4: "Market conform offer",
        list_3_5: "Market conform prices",
        list_3_6: "Better operational results",
        list_3_7: "Higher customer satisfaction",
        list_3_8: "More quality",
        list_3_9: "More turnover",
        list_3_10: "More profit",
        list_3_11: "Time for other priorities",

        /*FOUR*/
        four_1_title: "Extensive experience in catering",
        four_1_body_1: "More than 20 years of experience in operational and commercial positions",
        four_1_body_2: "with several international caterers in Belgium and the Netherlands.",
        four_1_body_3: "Functions & responsibilities as business manager, commercial director, operations manager, ...",
        four_2_title: "Extensive experience in hospitality",
        four_2_body_1: "Over 12 years of experience in operational positions",
        four_2_body_2: "in several international hotel brands and in an international leisure park.",
        four_2_body_3: "Functions & responsibilities as F&B manager, banquet&event manager, head of restaurants, purchasing, cost control, restaurant MdH, kitchen & restaurant functions.",

        /*FIVE*/
        value_1_title: "Committed",
        value_1_content: "By empathizing with the client and his project, you gain commitment and engagement. You understand your client and his company culture. In this way you develop customer-oriented solutions that really add value.",
        value_2_title: "Sincere & straightforward",
        value_2_content: "The cliché says that consultants talk down to their clients. Clients who are investing desire much more than that. This takes courage. It is not obvious to tell the truth.",
        value_3_title: "Curious",
        value_3_content: "No standard solutions, but focus on innovation and/or proven methods. It all begins with an in-depth diagnosis, in which continuous &quot;why-questions&quot; and analysis are central. This way you automatically achieve innovative solutions",
        value_4_title: "Entrepreneurial & Team player",
        value_4_content: "With a thorough action plan, you meet deadlines, deliver top quality and monitor progress. Looking forward to meet people who find satisfaction in realizing projects with others.",

        /*SIX*/
        contact_title: "Contact us",
        contact_subtitle: "Mail us or call us! We are happy to help you.",
        address: "Address",
        phone: "Phone",
        email: "Email",
    },
};

const languagePicker = document.querySelector("#language-picker-select");
languagePicker.addEventListener("change", event => {
    const selectedLanguage = data[event.target.value];
    Object.keys(languageElements).forEach(key => languageElements[key].innerHTML = selectedLanguage[key]);
});

languagePicker.value = initialLanguage;
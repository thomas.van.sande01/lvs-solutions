const elements = {
    nav: document.querySelector(".nav"),
    navOpenBtn: document.querySelector(".navOpenBtn"),
    navCloseBtn: document.querySelector(".navCloseBtn")
};

const toggleNav = action => {
    elements.nav.classList[action]("openNav");
    elements.nav.classList[action === 'add' ? 'remove' : 'add']("openSearch");
};

elements.navOpenBtn.addEventListener("click", () => toggleNav('add'));
elements.navCloseBtn.addEventListener("click", () => toggleNav('remove'));